module gitlab.com/OdinSQL/sogt

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.5.1 // indirect
	github.com/antchfx/htmlquery v1.2.2 // indirect
	github.com/antchfx/xmlquery v1.2.3 // indirect
	github.com/antchfx/xpath v1.1.4 // indirect
	github.com/disiqueira/gotree v1.0.0
	github.com/gocolly/colly/v2 v2.0.1
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/protobuf v1.4.0-rc.4
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	google.golang.org/appengine v1.6.5 // indirect
	google.golang.org/genproto v0.0.0-20200309141739-5b75447e413d // indirect
	google.golang.org/grpc v1.27.1
	google.golang.org/protobuf v1.20.1 // indirect
)
