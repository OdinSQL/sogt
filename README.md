# SpiderOak go test (sogt)

Spider oak go test

Coding challenge for spider oak. 

## External dependencies

### protobuf v3.11.4

https://github.com/protocolbuffers/protobuf/releases/tag/v3.11.4

### protoc-gen-go v1.20.0
go install google.golang.org/protobuf/cmd/protoc-gen-go

## Generating go code from proto
from the sogt project director run the following command

    protoc -I=rpc --go_out=plugins=grpc,paths=source_relative:rpc rpc/crawler.proto

This should result in the file crawler.pb.go being regenerated based on the crawler.proto

## Build instructions
First run 

    go mod tidy
    
from the project directory

then run

    go build -o bin ./cmd/crawler

and/or
    
    go build -o bin ./cmd/server
   

## Notes and thoughts

### Requirements gathering
The first thing I would normally do before jumping into writing code would be to ask as many relevant questions as 
possible regarding the purpose of the project, who will be using it and how would they like it to function.

While coding I would normally continue to ask questions as they arise. 

### Time constraints
Due to time constraints several design decisions were made in order to focus on the core functionality requested.

### Build tools
At Mattel we were using Mage (https://github.com/magefile/mage ) to manage the tool chain dependencies, building docker 
containers and ensuring that everyone was using the same version of go.
I found it useful when dealing with larger projects where multiple developers were contributing.

### Colly a framework for web scrapers
After a bit of research I've decided to use colly http://go-colly.org/  
As this is a test/demo project I've decided to use the in memory storage. If I were going to build a web scraper for any 
other use I would likely use postgress as the backend storage, eventually the solution that I've come up with would run
out of memory as I'm storing data in a map.

Other storage  options are demonstrated here : https://github.com/zolamk/colly-postgres-storage

If you would like to to see backed by a DB just let me know.

### Subdomains
Subdomains, they exist and a given domain may contain subdomains I've taken this into account. 
If this was a more complete project this could have been a command line argument.

### Circular references
Most sites contain circular references Page #1 links to Page #2, and Page #2 links back to page #1
I've decided not to log the same link more than once to avoid the circular reference issue. An alternative approach 
would be to allow circular references but to limit the depth of the site tree.

Due to the fact that each page is only included once to avoid circular references and multiple pages are being loaded in parallel
The tree that is returned and printed will be accurate but may not display as you would expect it to display.
If the same link appears on two pages it will only be followed once on whichever page is indexed first.

### HTTP Errors
HTTP Errors could occur while indexing a site. The requirements did not specify that they should be returned, 
however I have an errors collection defined in the crawler.proto.

### Docker
If you would like to have me create a docker file for this just let me know.

### Do not follow links that are not from the same domain
I'm saving the node to the site tree at the time of the request which means that non-indexed pages are not included in the tree.
This was a conscious decision on my part but may not be what was intended. 

### Unit tests
Time has been a factor and I haven't written any. If you would like me to add a few just let me know.



