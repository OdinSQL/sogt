package main

import (
	"gitlab.com/OdinSQL/sogt/rpc"
	"google.golang.org/grpc"
	"log"
	"net"
	"time"
)

type app struct {
	c    *crawler
	addr string
}

func main() {
	a := app{}
	// I would normally put in a config file or environment variable
	a.c = NewCrawler(true, 8, time.Duration(time.Second*15))
	a.addr = "localhost:8182"
	serve(&a)
}

func serve(a *app) {

	listen, err := net.Listen("tcp", a.addr)
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}
	log.Printf("listening on %s\n", listen.Addr())
	s := server{app: a}

	grpcServer := grpc.NewServer()
	rpc.RegisterSpiderOakCrawlerServer(grpcServer, s)

	err = grpcServer.Serve(listen)
	if err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}
