package main

import (
	"fmt"
	"github.com/gocolly/colly/v2"
	"gitlab.com/OdinSQL/sogt/rpc"
	"log"
	"net/url"
	"strings"
	"sync"
	"time"
)

type crawler struct {
	collectors     map[string]*collectorInfo
	async          bool
	requestLimit   uint16
	requestTimeout time.Duration
	collectorsLock sync.Mutex
}

type collectorInfo struct {
	siteTree   *rpc.ListResponse_SiteTree
	processing bool
	stop       bool
	infoLock   sync.Mutex
}

type pageNode struct {
	url  string
	node []pageNode
}

func NewCrawler(async bool, requestLimit uint16, requestTimeout time.Duration) *crawler {
	c := crawler{async: async, requestLimit: requestLimit, requestTimeout: requestTimeout}
	c.collectors = make(map[string]*collectorInfo)
	c.collectorsLock = sync.Mutex{}
	return &c
}

func getDomain(inURL string) (string, error) {
	u, err := url.Parse(inURL)
	if err != nil {
		return "", err
	}
	parts := strings.Split(u.Hostname(), ".")
	//stay in the same domain including sub domains
	if len(parts) >= 2 {
		return parts[len(parts)-2] + "." + parts[len(parts)-1], nil
	}
	return "", nil
}

func findStringInSlice(stringsToSearch []string, valueToFind string) bool {
	for _, item := range stringsToSearch {
		if item == valueToFind {
			return true
		}
	}
	return false
}

func getParentNode(ctx *colly.Context) *rpc.ListResponse_SiteTree_PageNode {
	interfaceVal := ctx.GetAny("ParentNode")
	if interfaceVal != nil {
		return interfaceVal.(*rpc.ListResponse_SiteTree_PageNode)
	} else {
		return nil
	}
}

func (c *crawler) Start(rootURL *url.URL) error {
	c.collectorsLock.Lock()
	defer c.collectorsLock.Unlock()

	log.Printf("start called for %s", rootURL.String())
	collector := colly.NewCollector(colly.Async(c.async))

	// one approach to dealing with circular references
	collector.AllowURLRevisit = false

	// a secondary approach to dealing with circular references would be to limit the MaxDepth
	// collector.MaxDepth = 10

	info, ok := c.collectors[rootURL.String()]
	// we don't want two crawls occurring on the same site at the same time
	if !ok || !info.processing {
		info = &collectorInfo{processing: true, stop: false, siteTree: &rpc.ListResponse_SiteTree{}, infoLock: sync.Mutex{}}
		c.collectors[rootURL.String()] = info
	} else {

		return fmt.Errorf("an existing crawl request is  already being processed for the url %s", rootURL.String())
	}

	domain, err := getDomain(rootURL.String())
	info.infoLock.Lock()
	if err != nil {
		collector.AllowedDomains = []string{domain}
	} else {
		collector.AllowedDomains = []string{rootURL.Host}
	}
	info.infoLock.Unlock()

	// limit requests because we don't want to kill the server
	err = collector.Limit(&colly.LimitRule{Parallelism: int(c.requestLimit), DomainRegexp: ".*"})
	if err != nil {
		log.Printf(err.Error())
	}
	collector.SetRequestTimeout(c.requestTimeout)

	/*
		q, err := queue.New(
			int(c.requestLimit),                         // Number of consumer threads
			&queue.InMemoryQueueStorage{MaxSize: 10000}, // Use default queue storage
		)
		if err != nil {
			return err
		}
	*/

	// Find and visit all links
	collector.OnHTML("a[href]", func(e *colly.HTMLElement) {
		info.infoLock.Lock()
		defer info.infoLock.Unlock()
		if info.stop {
			return
		}

		// this block allows subdomains in the same domain of the
		href := e.Attr("href")
		linkURL, err := url.Parse(href)
		if err != nil {
			//couldn't parse out the url from the href
		} else {
			linkDomain, err := getDomain(linkURL.String())
			if err != nil {
				// couldn't find the domain for  the url
			} else {
				if strings.ToUpper(linkDomain) == strings.ToUpper(domain) && !findStringInSlice(collector.AllowedDomains, linkURL.Host) {
					collector.AllowedDomains = append(collector.AllowedDomains, linkURL.Host)
				}
			}
		}
		// end code related to subdomains

		/*
			// Couldn't get context to work correctly when using a queue
			var b []byte
			newRequest, err := e.Request.New("GET", e.Attr("href"), bytes.NewReader(b))
			pNode := getParentNode(e.Response.Ctx)
			if pNode != nil {
				log.Println("in html")
				log.Println(reflect.TypeOf(pNode))
				newRequest.Ctx = colly.NewContext()
				newRequest.Ctx.Put("ParentNode", pNode)
			}
			err = q.AddRequest(newRequest)
		*/
		err = e.Request.Visit(e.Attr("href"))
		if err != nil {
			// this just prints out info about url's being already visited
			// log.Printf(err.Error())
		}
	})

	/*
		collector.OnScraped(func(r *colly.Response) {
			q.Run(collector)
		})
	*/
	collector.OnRequest(func(r *colly.Request) {
		info.infoLock.Lock()
		defer info.infoLock.Unlock()

		if info.stop {
			return
		}
		pnode := getParentNode(r.Ctx)
		newNode := &rpc.ListResponse_SiteTree_PageNode{Url: r.URL.String(), SubPages: []*rpc.ListResponse_SiteTree_PageNode{}}
		if pnode == nil {
			pnode = newNode
		}
		if info.siteTree.PageNode == nil {
			//first node
			info.siteTree.PageNode = newNode
		} else {
			pnode.SubPages = append(pnode.SubPages, newNode)
		}
		r.Ctx.Put("ParentNode", newNode)

		log.Println("Visiting", r.URL)

	})

	collector.OnError(func(r *colly.Response, err error) {
		c.collectorsLock.Lock()
		defer c.collectorsLock.Unlock()
		pnode := getParentNode(r.Ctx)
		if pnode == nil {
			log.Printf(err.Error())
			return
		}

		// how should redirects be represented?
		switch err.(type) {
		// handle redirects in the same domain but different sub domain
		case *url.Error:
			if err, ok := err.(*url.Error); ok {
				redirectURL, err := url.Parse(err.URL)
				if err != nil {
					pnode.CrawlErrors = append(pnode.CrawlErrors, &rpc.ListResponse_SiteTree_PageNode_CrawlError{Message: err.Error()})
					return
				}
				redirectDomain, err := getDomain(redirectURL.String())
				if err != nil {
					pnode.CrawlErrors = append(pnode.CrawlErrors, &rpc.ListResponse_SiteTree_PageNode_CrawlError{Message: err.Error()})
					return
				}
				if strings.ToUpper(redirectDomain) == strings.ToUpper(domain) && !findStringInSlice(collector.AllowedDomains, redirectURL.Host) {
					collector.AllowedDomains = append(collector.AllowedDomains, redirectURL.Host)
					//try it again
					// had an issue with the context being cleared when using visit
					err = r.Request.Visit(redirectURL.String())
					if err != nil {
						log.Printf(err.Error())
					}
					//collector.Visit(redirectURL.String())
					return
				}
			} else {
				pnode.CrawlErrors = append(pnode.CrawlErrors, &rpc.ListResponse_SiteTree_PageNode_CrawlError{Message: err.Error()})
				return
			}
		default:
			pnode.CrawlErrors = append(pnode.CrawlErrors, &rpc.ListResponse_SiteTree_PageNode_CrawlError{Message: err.Error()})
			return
		}
	})

	go func() {
		err := collector.Visit(rootURL.String())
		if err != nil {
			log.Printf(err.Error())
		}
		// wait until all requests have finished
		collector.Wait()

		//update collection info with finished
		info.infoLock.Lock()
		defer info.infoLock.Unlock()
		info.processing = false

		log.Printf("finished crawling site %s", rootURL.String())
	}()

	return nil

}

// had considered doing this with a channel but it was easier to do with without  it
// if the stop needed to be more immediate I could use a channel to raise a panic and then recover
func (c *crawler) Stop(url *url.URL) error {
	c.collectorsLock.Lock()
	defer c.collectorsLock.Unlock()
	log.Printf("stop called for %s", url.String())
	info, ok := c.collectors[url.String()]
	if !ok {
		return fmt.Errorf("no crawl data for the url : %s", url.String())
	}
	info.infoLock.Lock()
	defer info.infoLock.Unlock()
	if !info.processing {
		return fmt.Errorf("crawl of url : '%s' has already stoped", url.String())
	}
	log.Printf("stopping crawl of url %s", url.String())
	info.stop = true
	info.processing = false
	return nil
}

func (c *crawler) List(url *url.URL) (*rpc.ListResponse, error) {
	c.collectorsLock.Lock()
	defer c.collectorsLock.Unlock()
	response := rpc.ListResponse{SiteTrees: make(map[string]*rpc.ListResponse_SiteTree), TimeStamp: getTimeStamp()}
	if url != nil {
		log.Printf("list called for %s", url.String())
		info, ok := c.collectors[url.String()]
		if !ok {
			return &response, fmt.Errorf("no crawl data for the url : %s", url.String())
		}
		info.infoLock.Lock()
		defer info.infoLock.Unlock()
		response.SiteTrees[url.String()] = info.siteTree
		return &response, nil
	} else {
		log.Println("list called")
	}

	for key, val := range c.collectors {
		response.SiteTrees[key] = val.siteTree
	}

	return &response, nil
}
