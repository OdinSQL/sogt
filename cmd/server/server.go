package main

import (
	"context"
	"github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/OdinSQL/sogt/rpc"
	"net/url"
	"time"
)

type server struct {
	rpc.UnimplementedSpiderOakCrawlerServer
	*app
}

func getTimeStamp() *timestamp.Timestamp {
	now := time.Now()
	return &timestamp.Timestamp{Seconds: now.Unix(), Nanos: int32(now.Nanosecond())}
}

func getURL(urlToParse string) (*url.URL, error) {
	u, err := url.Parse(urlToParse)
	if err != nil {
		return nil, err
	}
	// if the schema is missing try http
	if u.Scheme == "" {
		u.Scheme = "http"
		// parsing a url without a schema does not populate the host field
		// this recursion will result in a populated host when the schema is omitted
		return getURL(u.String())
	}
	return u, nil
}

// most endpoints would usually be protected with some kind of token returned by an authentication method
func (s server) Start(c context.Context, request *rpc.ActionRequest) (*rpc.Response, error) {
	u, err := getURL(request.Url)
	if err != nil {
		return nil, err
	}
	err = s.c.Start(u)
	if err != nil {
		return nil, err
	}

	r := rpc.Response{Ok: true, Response: "start request received", TimeStamp: getTimeStamp()}
	return &r, nil
}

func (s server) Stop(c context.Context, request *rpc.ActionRequest) (*rpc.Response, error) {
	u, err := getURL(request.Url)
	if err != nil {
		return nil, err
	}
	err = s.app.c.Stop(u)
	if err != nil {
		return nil, err
	}
	r := rpc.Response{Ok: true, Response: "stop request received", TimeStamp: getTimeStamp()}
	return &r, nil
}

func (s server) List(c context.Context, request *rpc.ActionRequest) (*rpc.ListResponse, error) {
	if request.Url != "" {
		u, err := getURL(request.Url)
		if err != nil {
			return nil, err
		}
		response, err := s.app.c.List(u)
		if err != nil {
			return nil, err
		}
		return response, nil
	}
	// pass a nil url pointer to receive all site trees
	response, err := s.app.c.List(nil)
	if err != nil {
		return nil, err
	}
	return response, nil

}
