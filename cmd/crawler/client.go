package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/disiqueira/gotree"
	"gitlab.com/OdinSQL/sogt/rpc"
	"google.golang.org/grpc"
	"log"
	"strings"
)

func main() {
	// normally would be passed in as a parameter or from config
	serverAddress := "localhost:8182"
	conn, err := grpc.Dial(serverAddress, grpc.WithInsecure())

	if err != nil {
		log.Fatalf("Could not connect  to grpc server at '%s' error : %s", serverAddress, err.Error())
	}
	if conn != nil {
		defer func() {
			if err := conn.Close(); err != nil {
				log.Fatalf("Could not close connection to grpc server at '%s' error : %s", serverAddress, err.Error())
			}
		}()
	}

	client := rpc.NewSpiderOakCrawlerClient(conn)

	var startSt, stopSt string
	var listB bool

	flag.StringVar(&startSt, "start", "", "crawl -start site.com")
	flag.StringVar(&stopSt, "stop", "", "crawl -stop site.com")
	// url has been set to be optional for list if omitted it will return all site trees
	flag.BoolVar(&listB, "list", true, "crawl -list (optional) site.com")

	flag.Parse()

	if isFlagPassed("start") {
		err = start(client, startSt)
	} else if isFlagPassed("stop") {
		err = stop(client, stopSt)
	} else if isFlagPassed("list") {
		url := ""
		if len(flag.Args()) > 0 {
			url = flag.Arg(0)
		}
		err = list(client, url)
	}

	if err != nil {
		fmt.Printf("Error : %s\n", err.Error())
	}
}

func isFlagPassed(name string) bool {
	found := false
	flag.Visit(func(f *flag.Flag) {
		if f.Name == name {
			found = true
		}
	})
	return found
}

func start(c rpc.SpiderOakCrawlerClient, url string) error {
	rec := rpc.ActionRequest{Url: strings.TrimSpace(url)}
	if rec.Url == "" {
		return fmt.Errorf("no url has been provided")
	}
	_, err := c.Start(context.Background(), &rec)
	if err != nil {
		return err
	}
	return nil
}

func stop(c rpc.SpiderOakCrawlerClient, url string) error {
	rec := rpc.ActionRequest{Url: strings.TrimSpace(url)}
	if rec.Url == "" {
		return fmt.Errorf("no url has been provided")
	}
	_, err := c.Stop(context.Background(), &rec)
	if err != nil {
		return err
	}
	return nil
}

func list(c rpc.SpiderOakCrawlerClient, url string) error {
	rec := rpc.ActionRequest{Url: strings.TrimSpace(url)}
	resp, err := c.List(context.Background(), &rec)
	if err != nil {
		return err
	}

	printFormattedSiteTree(resp.SiteTrees)

	return nil
}

func printFormattedSiteTree(s map[string]*rpc.ListResponse_SiteTree) {
	trees := gotree.New("Site Trees")

	for _, siteTree := range s {
		buildTree(siteTree.PageNode, trees)
	}

	fmt.Println(trees.Print())

}

//yay recursion
func buildTree(n *rpc.ListResponse_SiteTree_PageNode, t gotree.Tree) {
	treeNode := t.Add(n.Url)
	for _, sub := range n.SubPages {
		buildTree(sub, treeNode)
	}
}
